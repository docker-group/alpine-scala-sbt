FROM registry.gitlab.com/docker-group/alpine-oracle-java:12.0.1

ENV SCALA_VERSION=1.2.8 \
    SCALA_HOME=/usr/share/scala

# NOTE: bash is used by scala/scalac scripts, and it cannot be easily replaced with ash.

RUN apk add --no-cache --virtual=.build-dependencies wget ca-certificates && \
    apk add --no-cache bash && \
    cd "/tmp" && \
    wget -q "https://sbt-downloads.cdnedge.bluemix.net/releases/v${SCALA_VERSION}/sbt-${SCALA_VERSION}.tgz" && \
    tar xzf "sbt-${SCALA_VERSION}.tgz" && \
    mkdir "${SCALA_HOME}" && \
    rm "/tmp/sbt/bin/"*.bat && \
    mv "/tmp/sbt/bin" "/tmp/sbt/lib" "${SCALA_HOME}" && \
    ln -s "${SCALA_HOME}/bin/"* "/usr/bin/" && \
    apk del .build-dependencies && \
    rm -rf "/tmp/"*
